const router= require('express').Router();
const auth = require('../controller/auth')

router.post('/register', auth.Register)
router.post('/login', auth.Login)

module.exports= router
