const router= require('express').Router();
const verified = require('../middleware/verifyToken');
const blog = require('../controller/posts');
const { upload } = require('../middleware/multer');


router.get('/', verified, blog.getAll)
router.get('/:id', blog.getOne)
router.post('/', verified, upload, blog.createPost )
router.put('/:id', upload, blog.editPost)

module.exports = router
