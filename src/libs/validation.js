const Joi = require('joi')

 const validator = (schema) => (payload) => 
 schema.validate(payload, {abortEarly : false})

 const registerSchema = Joi.object({
    name: Joi.string().min(6).required(),
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required(),
})
 const loginSchema = Joi.object({
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required(),
})


exports.registerValidator = validator(registerSchema)
exports.loginValidator = validator(loginSchema)

