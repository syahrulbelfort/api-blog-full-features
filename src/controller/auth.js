const registerUser = require('../services/auth/register')
const loginUser = require('../services/auth/login')

module.exports={
    Register : async (req,res)=>{
        try {
            const result = await registerUser(req.body)
            res.send(result)
        } catch (error) {
            console.error(error)
        }
    },
    Login : async (req,res)=>{
        try {
            const result = await loginUser(req.body);
            res.send(result)
        } catch (error) {
            console.error(error)
        }
    }
}
