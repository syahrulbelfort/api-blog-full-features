const blog = require('../services/CRUD/index');


module.exports={
    getAll : async(req,res)=>{
        const result = await blog.getAllBlogs()
        res.send(result)
    },
    getOne : async(req,res)=>{
        const result = await blog.getOneBlog(req.params.id)
        res.send(result)
    },
    createPost : async (req,res)=>{
        const {title, content} = req.body
        const result = await blog.createOneBlog(
            title,
            content,
            req.file.path,
            req.user.id
        )
        res.send(result)
    },
    editPost : async(req, res) =>{
        const {title, content} = req.body
        const result = await blog.editOneBlog(
            req.params.id,
            title,
            content,
            req.file.path
            )
        res.send(result) 
    }
}
