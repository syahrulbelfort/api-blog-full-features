const bcrypt = require('bcryptjs');
const User = require('../../model/user');
const { registerValidator } = require('../../libs/validation')

exports.registerUser = async (userData) => {
  // VALIDATE DATA 
  const { error } = registerValidator(userData);
  if (error) {
    throw new Error(error.details[0].message);
  }

  //CHECK IF USER ALREADY EXISTS
  const emailExist = await User.findOne({ email: userData.email });
  if (emailExist) {
    throw new Error('Email already exists');
  }

  //HASH THE PASSWORD
  const salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(userData.password, salt);

  //CREATE A NEW USER
  const user = new User({
    name: userData.name,
    email: userData.email,
    password: hashPassword,
  });

  const savedUser = await user.save();

  return {
    user: savedUser._id,
    success: 'Registration successful!',
  };
};
