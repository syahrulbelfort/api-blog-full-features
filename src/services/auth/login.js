
const bcrypt = require('bcryptjs');
const User = require('../../model/user');
const { loginValidator } = require('../../libs/validation')


exports.loginUser = async (req,res)=>{
    // VALIDATE DATA 
       const {error } = loginValidator(req.body)
       if(error) return res.send(error.details[0].message)

     //CHECK USER IF ALREADY EXIST
    const user = await User.findOne({email: req.body.email})
    if(!user) return res.status(400).send('Email is not found')

    const validPass = await bcrypt.compare(req.body.password, user.password)
    if(!validPass) return res.status(200).send('Password salah!')

    //CREATE AND ASSIGN TOKEN
    const payload = {
        id : user._id
    }

    const token = jwt.sign(payload, process.env.TOKEN_SECRET)
    res.header('auth-token', token).send(token)

}