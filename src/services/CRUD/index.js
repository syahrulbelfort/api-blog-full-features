const BlogPost = require('../../model/blog');


module.exports ={
    getAllBlogs :  async (req,res)=>{
        try {
            const blogPosts = await BlogPost.find()
            return{
                message : 'List Blogs',
                data : blogPosts
            }
        } catch (error) {
            console.error(error)
            throw new Error('Server error!')
        }
    },
    getOneBlog :  async (userId)=>{
        try {
            const getOne = await BlogPost.findById(userId)
            return getOne
        } catch (error) {
            console.error(error)
        }
    },
    createOneBlog : async (title, content, imagePath, authorId)=>{
        try {
            const createOne = await BlogPost.create({
               title,
               content,
               image : imagePath,
               author : authorId 
            })
            return createOne
        } catch (error) {
            
        }
    },
    editOneBlog : async (blogId, title, content, imagePath) => {
        try {
          const editOne = await BlogPost.findByIdAndUpdate(
            blogId,
            {
              $set: {
                title,
                content,
                image: imagePath
              }
            },
          );
          return editOne;
        } catch (error) {
          console.error(error);
        }
      }
}

