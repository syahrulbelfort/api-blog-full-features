const multer = require('multer');

const fileStorage = multer.diskStorage({
    destination: (err,file,cb)=>{
        cb(null, 'images')
    },
    filename : (req, file, cb)=>{
        cb(null, new Date().getTime() + '-' + file.originalname)
    }
})

const fileFilter = (req,file,cb)=>{
    if(file.mimetype == 'image/jpeg' || file.mimetype == 'image/png' || file.mimetype == 'image/jpg'){
        cb(null,true)
    } else{
        cb(null, false)
    }
}

const upload = multer({storage : fileStorage, filter : fileFilter}).single('image')

module.exports = { upload }