const mongoose = require('mongoose');
const {Schema} = mongoose

const blogSchema = new Schema({
   title:{
    type : String,
    required : true,
    min : 6
   },
   content: {
    type: String,
    required : true,
    min : 10
   },
   image: {
    type: String,
    required : true, 
  },
   author:{
    type: mongoose.Schema.Types.ObjectId,
    ref : "User",
    required : true
   },
   createdAt: {
    type: Date,
    default: Date.now,
  },
})

const blogPost = mongoose.model('Blog', blogSchema);

module.exports = blogPost;

