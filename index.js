require('dotenv').config()
const express = require('express');
const app = express();
const authRoute = require('./src/router/auth');
const postRoute = require('./src/router/posts');
const mongoose = require('mongoose');
const PORT = process.env.PORT
const methodOverride = require('method-override')


//middleware
app.use(express.json())
app.use(methodOverride(`_method`))
app.use('/image',express.static('images'))


//connect to DB
async function connectDB(){
    try {
        await mongoose.connect(process.env.SECRET_URL,
            {
            useNewUrlParser: true,
           },
        console.log(`database connected!`)
        )
    } catch (error) {
        console.error(error)
    }
}

connectDB()

// route
app.use(`/api/user`, authRoute)
app.use(`/api/post`, postRoute)

app.listen(PORT, ()=>{
    console.log(`server is running on ${PORT}`)
})
